package fr.nitorac.minelia.redis;

import com.google.gson.JsonElement;

/**
 * Created by Nitorac on 16/04/2017.
 */
public interface IMNReceiver {
    void onMessage(String subchannel, JsonElement msg);
}
