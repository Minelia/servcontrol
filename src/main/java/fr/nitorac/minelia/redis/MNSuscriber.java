package fr.nitorac.minelia.redis;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import fr.nitorac.minelia.Csts;
import fr.nitorac.minelia.Log;
import fr.nitorac.minelia.ServControlApplication;
import fr.nitorac.minelia.Utils;
import redis.clients.jedis.JedisPubSub;

/**
 * Created by Nitorac on 15/04/2017.
 */
public class MNSuscriber extends JedisPubSub {
    @Override
    public void onMessage(String channel, final String msg) {
        // Doit être exécuté dans le process du serveur !
        if(!channel.equals(Csts.REDIS_MAIN_CHANNEL)) {
            Log.WARN.log("Channel Redis malformé (" + channel + " : " + msg + ")");
            return;
        }
        try {
            JsonObject obj = new JsonParser().parse(msg).getAsJsonObject();
            if(Utils.hasValIn2Decomp(obj.get(Csts.REDIS_TYPE_JSONKEY).getAsInt(), RedisManager.CH_SERVCONTROL)){
                ServControlApplication.getRedisManager().callAll(obj.get(Csts.REDIS_TITLE_JSONKEY).getAsString(), obj.get(Csts.REDIS_MSG_JSONKEY));
            }
        } catch (Exception e) {
            Log.WARN.log("Message Redis malformé (" + channel + " : " + msg + ")");
            e.printStackTrace();
            return;
        }
    }

    @Override
    public void onPMessage(String s, String s2, String s3) {

    }

    @Override
    public void onSubscribe(String s, int i) {

    }

    @Override
    public void onUnsubscribe(String s, int i) {

    }

    @Override
    public void onPUnsubscribe(String s, int i) {

    }

    @Override
    public void onPSubscribe(String s, int i) {

    }
}
