package fr.nitorac.minelia.redis;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import fr.nitorac.minelia.Csts;
import fr.nitorac.minelia.Log;
import fr.nitorac.minelia.ServControlApplication;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Nitorac on 15/04/2017.
 */
public class RedisManager {

    private MNSuscriber suscriber;
    private JedisPool pool;
    private static Map<String, IMNReceiver> receivers;

    /*********** APPEL DES CHANNELS *************/
    public static final int CH_SPIGOT = 1;
    public static final int CH_BUNGEE = 2;
    public static final int CH_SERVCONTROL = 4;
    public static final int CH_DISCORD = 8;

    /******************
     *
     * Ici, je donne des nombres en puissances de 2 aux channels pour autoriser l'envoi d'un message à plusieurs channels
     * Par exemple :    send(7, ..., ...);
     *
     * 7 = 4 + 2 + 1    donc ca veut dire qu'on envoie au ServControl, Bungee, et Spigot
     *
     * Ainsi pour l'utiliser, il suffit juste de faire :
     *
     *          send(CH_SPIGOT + CH_BUNGEE + CH_SERVCONTROL, ..., ...);
     *
     *      (Pour plus d'infos, tu peux aller voir comment décomposer un nombre décimal en binaire ^^)
     */

    public RedisManager(){
        receivers = new HashMap<>();
        pool = new JedisPool(new JedisPoolConfig(), ServControlApplication.getConfigManager().get(Csts.GENERAL_CONF).getString("redis.ip", "127.0.0.1"),
            ServControlApplication.getConfigManager().get(Csts.GENERAL_CONF).getInt("redis.port", 6379), 0,
            ServControlApplication.getConfigManager().get(Csts.GENERAL_CONF).getString("redis.password", "t5GFzjSVr31H451zKx"));
    }

    public void init(){
        Runnable run = () -> {
            suscriber = new MNSuscriber();
            Jedis jedis = pool.getResource();
            try {
                jedis.subscribe(suscriber, Csts.REDIS_MAIN_CHANNEL);
            } catch (Exception e) {
                Log.ERROR.log("Impossible de se connecter au serveur Redis !");
                e.printStackTrace();
            }finally {
                jedis.close();
            }
        };

        new Thread(run).start();
    }

    public void registerReceiver(String key, IMNReceiver receiver){
        receivers.putIfAbsent(key, receiver);
    }

    public void send(int type, final String channelTitle, final Object message){
        Runnable run = () -> {
            Jedis jedis = pool.getResource();
            JsonElement elem = (new GsonBuilder().create()).toJsonTree(message, message.getClass());
            JsonObject obj = new JsonObject();
            obj.addProperty(Csts.REDIS_TYPE_JSONKEY, type);
            obj.addProperty(Csts.REDIS_TITLE_JSONKEY, channelTitle);
            obj.add(Csts.REDIS_MSG_JSONKEY, elem);

            try {
                jedis.publish(Csts.REDIS_MAIN_CHANNEL, obj.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }finally {
                jedis.close();
            }
        };

        new Thread(run).start();
    }

    public void replaceReceiver(String key, IMNReceiver newRec){
        receivers.replace(key, newRec);
    }

    /*******
     * ATTENTION, A NE JAMAIS UTILISER
     *
     * @param subchannel ATTENTION, A NE JAMAIS UTILISER
     * @param msg ATTENTION, A NE JAMAIS UTILISER
     */
    public void callAll(String subchannel, JsonElement msg){
        receivers.forEach((key, rec) -> rec.onMessage(subchannel, msg));
    }

    public void unregisterReceiver(String key){
        receivers.remove(key);
    }
}
