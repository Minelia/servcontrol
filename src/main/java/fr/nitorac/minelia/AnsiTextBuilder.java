package fr.nitorac.minelia;

import org.springframework.boot.ansi.AnsiColor;

public class AnsiTextBuilder {

    private StringBuilder sb = new StringBuilder();

    public AnsiTextBuilder(){
        color(AnsiColor.DEFAULT);
    }

    public AnsiTextBuilder append(String text){
        sb.append(text);
        return this;
    }

    //@TODO: A FIX, CA NE MARCHE PAS ENCORE !!!

    public AnsiTextBuilder setPos(int pos){
        int current = sb.toString().length()-1;
        int diff = pos - current;
        if(diff > 0){
            append(" ", diff);
        }
        return this;
    }

    public AnsiTextBuilder append(String text, int repeat){
        if(repeat <= 0){
            return this;
        }else{
            append(text);
            return append(text, repeat-1);
        }
    }

    public AnsiTextBuilder color(AnsiColor color){
        sb.append(Utils.ansi(color));
        return this;
    }

    public String build(){
        return sb.toString();
    }
}
