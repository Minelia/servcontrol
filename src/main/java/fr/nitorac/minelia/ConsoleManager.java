package fr.nitorac.minelia;

public class ConsoleManager {

    private String activeServerKey;

    public ConsoleManager(){
        this.activeServerKey = "";
    }

    public boolean enableConsole(String key){
        Server serv;
        if((serv = ServControlApplication.servers.get(key)) == null || !serv.isAlive()){
            return false;
        }
        activeServerKey = key;
        serv.startOutputMonitor();
        serv.bindOutputToScreen(true);
        return true;
    }

    public void disableConsole(){
        ServControlApplication.servers.get(activeServerKey).unbindOutputToScreen();
        activeServerKey = "";
    }

    public boolean disableConsole(String key){
        if(activeServerKey == null || activeServerKey.isEmpty() || !key.equalsIgnoreCase(activeServerKey)){
            return false;
        }
        disableConsole();
        return true;
    }

    public String getActiveServerName(){
        return activeServerKey;
    }

    public boolean isConsoleActive(){
        return activeServerKey != null && !activeServerKey.isEmpty();
    }

    public Server getActiveServer(){
        return ServControlApplication.servers.getOrDefault(activeServerKey, null);
    }
}
