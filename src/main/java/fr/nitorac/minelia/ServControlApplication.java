package fr.nitorac.minelia;

import fr.nitorac.minelia.configuration.ConfigManager;
import fr.nitorac.minelia.springoverrides.JLineConfig;
import fr.nitorac.minelia.springoverrides.MNStandardAPIAutoConfiguration;
import fr.nitorac.minelia.springoverrides.ShellConfig;
import fr.nitorac.minelia.configuration.MNConfig;
import fr.nitorac.minelia.exceptions.NoJarFoundException;
import fr.nitorac.minelia.nio.ServerHandler;
import fr.nitorac.minelia.redis.RedisManager;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.SelfSignedCertificate;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.shell.SpringShellAutoConfiguration;
import org.springframework.shell.jline.JLineShellAutoConfiguration;
import org.springframework.shell.standard.StandardAPIAutoConfiguration;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableAutoConfiguration(exclude={JLineShellAutoConfiguration.class, SpringShellAutoConfiguration.class, StandardAPIAutoConfiguration.class})
@Import({JLineConfig.class, ShellConfig.class, MNStandardAPIAutoConfiguration.class})
@ComponentScan
public class ServControlApplication {

    public static final int PORT = 35698;

    private static RedisManager redisManager;
    private static ConsoleManager consoleManager;
    private static ConfigManager configManager;

    public static Map<String, Server> servers;

    public static Channel channel;

	public static void main(String[] args) throws Exception{
        if (args.length > 0 && Arrays.asList(args).contains("noLaunch")) {
            return;
        }
        servers = new HashMap<>();
        try{
            Log.INFO.log("Dossier actuel : " + Utils.currentDir.getAbsolutePath());
            Log.INFO.newLine(2);
            Log.INFO.log("Lecture des fichiers de configuration ...");
            Log.INFO.newLine();
            configManager = new ConfigManager();
            configManager.registerConfig(Utils.currentDir + File.separator + "general.yml");
            Log.INFO.newLine();
            Log.INFO.log("Fichiers de configuration chargés !");
            Log.INFO.newLine(2);
            Log.INFO.log("Initialisation du Redis ...");
            Log.INFO.newLine();
            redisManager = new RedisManager();
            redisManager.init();
            Log.INFO.log("Redis chargé !");
            Log.INFO.newLine();
            Log.INFO.log("Récupération de la liste des serveurs ...");
            Log.INFO.newLine();
            parseServers();
            Log.INFO.newLine();
            Log.INFO.log("Serveurs analysés (" + servers.size() + " trouvés) ...");
            Log.INFO.newLine();
            Log.INFO.log("Initialisation de la connexion serveur ...");
            setupNio();
            Log.INFO.newLine();
            Log.INFO.log("Connexion du serveur établie !");
            Log.INFO.newLine();
            Log.INFO.log("Console initialisée !");
        }catch (Exception e){
            e.printStackTrace();
        }

        consoleManager = new ConsoleManager();
        String[] disabledCommands = {"--spring.shell.command.quit.enabled=false", "--spring.shell.command.help.enabled=false"};
        String[] fullArgs = StringUtils.concatenateStringArrays(args, disabledCommands);
        SpringApplication app = new SpringApplication(ServControlApplication.class);
        app.setBannerMode(Banner.Mode.OFF);
        System.out.println();
        System.out.println();
        System.out.println("███╗   ███╗██╗███╗   ██╗███████╗██╗     ██╗ █████╗ ");
        System.out.println("████╗ ████║██║████╗  ██║██╔════╝██║     ██║██╔══██╗");
        System.out.println("██╔████╔██║██║██╔██╗ ██║█████╗  ██║     ██║███████║");
        System.out.println("██║╚██╔╝██║██║██║╚██╗██║██╔══╝  ██║     ██║██╔══██║");
        System.out.println("██║ ╚═╝ ██║██║██║ ╚████║███████╗███████╗██║██║  ██║");
        System.out.println("╚═╝     ╚═╝╚═╝╚═╝  ╚═══╝╚══════╝╚══════╝╚═╝╚═╝  ╚═╝");
        System.out.println();
        System.out.println();
        ConfigurableApplicationContext context = app.run(fullArgs);
    }

    public static void parseServers() throws Exception{
        File[] dirs = Utils.currentDir.listFiles(File::isDirectory);
        if(dirs == null){
            throw new FileNotFoundException("Impossible de trouver un dossier de serveur.");
        }

        Arrays.stream(dirs).forEach(d -> {
            File[] subfiles = d.listFiles();
            boolean pluginsDirFound = false;
            boolean jarFound = false;
            for(File f : subfiles){
                //Première vérif : contient un dossier "plugins"
                if(f.isDirectory() && f.getName().equals("plugins")){
                    pluginsDirFound = true;
                }
                //Deuxième vérif : contient au moins un jar
                if(f.isFile() && f.getName().endsWith(".jar")){
                    jarFound = true;
                }
            }
            if(!pluginsDirFound || !jarFound){
                return;
            }
            try {
                servers.put(d.getName(), new Server(d));
            } catch (NoJarFoundException e) {
                e.printStackTrace();
            }
        });
    }

    public static void setupNio() throws Exception{
        final SslContext sslCtx;
        SelfSignedCertificate ssc = new SelfSignedCertificate();
        sslCtx = SslContextBuilder.forServer(ssc.certificate(), ssc.privateKey()).build();

        EventLoopGroup bossGroup = new NioEventLoopGroup(1);
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap b = new ServerBootstrap();
            b.group(bossGroup, workerGroup)
                .channel(NioServerSocketChannel.class)
                .handler(new LoggingHandler(LogLevel.INFO))
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    public void initChannel(SocketChannel ch) {
                        ChannelPipeline p = ch.pipeline();
                        p.addLast(sslCtx.newHandler(ch.alloc()));
                        p.addLast(
                            new ObjectEncoder(),
                            new ObjectDecoder(ClassResolvers.cacheDisabled(null)),
                            new ServerHandler());
                    }
                });

            // Bind and start to accept incoming connections.
            channel = b.bind(PORT).awaitUninterruptibly().channel();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static RedisManager getRedisManager(){
        return redisManager;
    }

    public static ConsoleManager getConsoleManager(){
	    return consoleManager;
    }

    public static ConfigManager getConfigManager(){
	    return configManager;
    }
}
