package fr.nitorac.minelia.commands;

import fr.nitorac.minelia.*;
import org.springframework.boot.ansi.AnsiColor;
import org.springframework.core.MethodParameter;
import org.springframework.shell.CompletionContext;
import org.springframework.shell.CompletionProposal;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import org.springframework.shell.standard.ValueProviderSupport;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@ShellComponent()
public class Commands{
    @ShellMethod(value = "Quitter la console", key = {"quit", "exit", "end"})
    public void quit() {
        Log.WARN.log("ATTENTION: Quitter le SC cause un downtime pendant tout son temps d'extinction !");
        if(ServControlApplication.servers.values().stream().anyMatch(Server::isAlive)){
            Log.WARN.log("Extinction de tous les serveurs ...");
            Log.WARN.log("Pour quitter complètement le programme, veuillez reéxecuter cette commande quand tous les serveurs sont éteints !");
            stopAll();
        }else{
            System.exit(0);
        }
    }

    @ShellMethod(key = {"list", "ls"}, value = "Affiche la liste de tous les serveurs")
    public void listServers(){
        Map<String, Boolean> servs = ServControlApplication.servers.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, v -> v.getValue().isAlive()));
        System.out.println((servs.size() > 1) ? "Les serveurs trouvés sont : " : "Le serveur trouvé est : ");
        System.out.println();
        System.out.println();
        int maxPos = Utils.getPosFromItemMax(servs.keySet());
        servs.forEach((name, isAlive) -> {
            System.out.println((new AnsiTextBuilder())
                .append("   - ")
                .color(AnsiColor.BRIGHT_BLUE).append(name)
                .setPos(maxPos + 3)
                .color(AnsiColor.WHITE).append(": ")
                .color((isAlive) ? AnsiColor.GREEN : AnsiColor.BRIGHT_RED).append((isAlive) ? "Allumé" : "Éteint")
                .build());
        });
    }

    @ShellMethod(value = "Affiche la console du serveur.\n        ATTENTION: ENVOYER LA COMMANDE 'exit' POUR QUITTER LA CONSOLE (SANS ETEINDRE LE SERV)\n", key = {"console", "c"})
    public void consoleServer(@ShellOption(valueProvider = ServerValueProvider.class, help = "Le serveur spécifié") String serverName){
        Optional<String> opt = ServControlApplication.servers.keySet().stream()
            .filter(s -> s.toLowerCase().startsWith(serverName.toLowerCase()))
            .findFirst();

        if(!opt.isPresent()){
            System.out.println((new AnsiTextBuilder())
                .color(AnsiColor.YELLOW).append("Impossible de trouver le serveur ")
                .color(AnsiColor.WHITE).append(serverName)
                .color(AnsiColor.YELLOW).append(" !")
                .build());
        }else{
            if(!ServControlApplication.getConsoleManager().enableConsole(opt.get())){
                System.out.println((new AnsiTextBuilder())
                    .color(AnsiColor.YELLOW).append("Le serveur ")
                    .color(AnsiColor.WHITE).append(serverName)
                    .color(AnsiColor.YELLOW).append(" n'est pas allumé !")
                    .build());
            }
        }
    }

    @ShellMethod(value = "Démarre tous les serveurs", key = "start-all")
    public void startAll(){
        ServControlApplication.servers.entrySet().stream().filter(e -> !e.getValue().isAlive()).forEach(e -> {
            try {
                e.getValue().startServer();
                System.out.println((new AnsiTextBuilder())
                    .color(AnsiColor.GREEN).append("Le serveur ")
                    .color(AnsiColor.WHITE).append(e.getKey())
                    .color(AnsiColor.GREEN).append(" a bien été démarré !")
                    .build());
            } catch (IOException e1) {
                e1.printStackTrace();
                System.out.println((new AnsiTextBuilder())
                    .color(AnsiColor.RED).append("Le serveur ")
                    .color(AnsiColor.WHITE).append(e.getKey())
                    .color(AnsiColor.RED).append(" n'a pas pu être démarré !")
                    .build());
            }
        });
    }

    @ShellMethod(value = "Démarre un serveur", key = "start")
    public void startServer(@ShellOption(valueProvider = ServerValueProvider.class, help = "Le serveur spécifié") String serverName, @ShellOption(defaultValue="2048", help = "Paramètre Java -Xmx pour la RAM (en Mo)") int xmxM, @ShellOption(defaultValue="2048", help = "Paramètre Java -Xms pour la RAM (en Mo)") int xmsM){
        Optional<String> opt = ServControlApplication.servers.keySet().stream()
            .filter(s -> s.toLowerCase().startsWith(serverName.toLowerCase()))
            .findFirst();

        if(!opt.isPresent()){
            System.out.println((new AnsiTextBuilder())
                .color(AnsiColor.YELLOW).append("Impossible de trouver le serveur ")
                .color(AnsiColor.WHITE).append(serverName)
                .color(AnsiColor.YELLOW).append(" !")
                .build());
        }else{
            Server serv = ServControlApplication.servers.get(opt.get());
            try{
                if(!serv.isAlive()){
                    serv.startServer(xmxM, xmsM);
                    System.out.println((new AnsiTextBuilder())
                        .color(AnsiColor.GREEN).append("Le serveur ")
                        .color(AnsiColor.WHITE).append(serverName)
                        .color(AnsiColor.GREEN).append(" a bien été démarré !")
                        .build());
                }else{
                    System.out.println((new AnsiTextBuilder())
                        .color(AnsiColor.YELLOW).append("Le serveur ")
                        .color(AnsiColor.WHITE).append(serverName)
                        .color(AnsiColor.YELLOW).append(" est déjà démarré !")
                        .build());
                }
            }catch (IOException e){
                System.out.println((new AnsiTextBuilder())
                    .color(AnsiColor.RED).append("Impossible de démarrer le serveur ")
                    .color(AnsiColor.WHITE).append(serverName)
                    .color(AnsiColor.RED).append(" !")
                    .build());
                e.printStackTrace();
            }
        }
    }

    @ShellMethod(value = "Arrête tous les serveurs", key = "stop-all")
    public void stopAll(){
        ServControlApplication.servers.entrySet().stream().filter(entry -> entry.getValue().isAlive()).forEach(entry -> {
            entry.getValue().getAliveMonitor().removeCallback(Server.QUIT_CONSOLE_ON_SERVER_EXIT);
            entry.getValue().getAliveMonitor().addCallback("ExitAllRequest", () -> {
                Log.INFO.logPrompt((new AnsiTextBuilder())
                    .color(AnsiColor.GREEN).append("Le serveur ")
                    .color(AnsiColor.WHITE).append(entry.getKey())
                    .color(AnsiColor.GREEN).append(" a bien été arrêté !")
                    .build());
            });
            try {
                entry.getValue().stopServer();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    @ShellMethod(value = "Arrête un serveur", key = "stop")
    public void stopServer(@ShellOption(valueProvider = ServerValueProvider.class, help = "Le serveur spécifié") String serverName){
        Optional<String> opt = ServControlApplication.servers.keySet().stream()
            .filter(s -> s.toLowerCase().startsWith(serverName.toLowerCase()))
            .findFirst();

        if(!opt.isPresent()){
            System.out.println((new AnsiTextBuilder())
                .color(AnsiColor.YELLOW).append("Impossible de trouver le serveur ")
                .color(AnsiColor.WHITE).append(serverName)
                .color(AnsiColor.YELLOW).append(" !")
                .build());
        }else{
            Server serv = ServControlApplication.servers.get(opt.get());
            try{
                if(serv.isAlive()){
                    serv.getAliveMonitor().removeCallback(Server.QUIT_CONSOLE_ON_SERVER_EXIT);
                    serv.getAliveMonitor().addCallback("ExitRequest", () -> {
                        Log.INFO.logPrompt((new AnsiTextBuilder())
                            .color(AnsiColor.GREEN).append("Le serveur ")
                            .color(AnsiColor.WHITE).append(serverName)
                            .color(AnsiColor.GREEN).append(" a bien été arrêté !")
                            .build());
                    });
                    serv.stopServer();
                }else{
                    System.out.println((new AnsiTextBuilder())
                        .color(AnsiColor.YELLOW).append("Le serveur ")
                        .color(AnsiColor.WHITE).append(serverName)
                        .color(AnsiColor.YELLOW).append(" n'est pas démarré !")
                        .build());
                }
            }catch (IOException e){
                System.out.println((new AnsiTextBuilder())
                    .color(AnsiColor.RED).append("Impossible d'arrêter le serveur ")
                    .color(AnsiColor.WHITE).append(serverName)
                    .color(AnsiColor.RED).append(" !")
                    .build());
                e.printStackTrace();
            }
        }
    }

    @Component
    public class ServerValueProvider extends ValueProviderSupport {

        @Override
        public boolean supports(MethodParameter methodParameter, CompletionContext completionContext) {
            return true;
        }

        @Override
        public List<CompletionProposal> complete(MethodParameter methodParameter, CompletionContext completionContext, String[] strings) {
            List<CompletionProposal> result = new ArrayList<>();

            String userInput = completionContext.currentWordUpToCursor();

            ServControlApplication.servers.keySet().stream()
                .filter(s -> s.toLowerCase().startsWith(userInput.toLowerCase()))
                .forEach(s -> result.add(new CompletionProposal(s)));

            return result;
        }
    }
}
