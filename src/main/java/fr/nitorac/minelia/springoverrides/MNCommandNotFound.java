package fr.nitorac.minelia.springoverrides;

import org.springframework.shell.CommandNotFound;

import java.util.List;
import java.util.stream.Collectors;

public class MNCommandNotFound extends CommandNotFound {
    private final List<String> words;

    public MNCommandNotFound(List<String> words) {
        super(words);
        this.words = words;
    }

    @Override
    public String getMessage() {
        return String.format("Aucune commande trouvée pour '%s'", String.join(" ", words));
    }
}
