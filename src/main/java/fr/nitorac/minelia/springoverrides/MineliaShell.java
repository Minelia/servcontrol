package fr.nitorac.minelia.springoverrides;

import fr.nitorac.minelia.Log;
import fr.nitorac.minelia.ServControlApplication;
import org.jline.utils.Signals;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.MethodParameter;
import org.springframework.core.annotation.AnnotationAwareOrderComparator;
import org.springframework.shell.*;
import org.springframework.shell.Utils;
import org.springframework.util.ReflectionUtils;

import javax.annotation.PostConstruct;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

public class MineliaShell extends Shell {

    private final ResultHandler resultHandler;

    /**
     * Marker object returned to signify that there was no input to turn into a command
     * execution.
     */
    public static final Object NO_INPUT = new Object();

    @Autowired
    protected ApplicationContext applicationContext;

    private Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    protected Map<String, MethodTarget> methodTargets = new HashMap<>();

    protected List<ParameterResolver> parameterResolvers;

    /**
     * Marker object to distinguish unresolved arguments from {@code null}, which is a valid
     * value.
     */
    protected static final Object UNRESOLVED = new Object();

    public MineliaShell(ResultHandler resultHandler) {
        super(resultHandler);
        this.resultHandler = resultHandler;
    }

    @Override
    public Map<String, MethodTarget> listCommands() {
        return methodTargets;
    }

    @PostConstruct
    public void gatherMethodTargets() throws Exception {
        ConfigurableCommandRegistry registry = new ConfigurableCommandRegistry();
        for (MethodTargetRegistrar resolver : applicationContext.getBeansOfType(MethodTargetRegistrar.class).values()) {
            resolver.register(registry);
        }
        methodTargets = registry.listCommands();
        methodTargets.values()
            .forEach(this::validateParameterResolvers);
    }

    @Autowired
    public void setParameterResolvers(List<ParameterResolver> resolvers) {
        this.parameterResolvers = new ArrayList<>(resolvers);
        AnnotationAwareOrderComparator.sort(parameterResolvers);
    }

    /**
     * The main program loop: acquire input, try to match it to a command and evaluate. Repeat
     * until a {@link ResultHandler} causes the process to exit or there is no input.
     * <p>
     * This method has public visibility so that it can be invoked by actual commands
     * (<em>e.g.</em> a {@literal script} command).
     * </p>
     */
    public void run(InputProvider inputProvider) throws IOException {
        Object result = null;
        while (true) {
            Input input;
            try {
                input = inputProvider.readInput();
            } catch (ExitRequest exitRequest){
                Log.WARN.log("ARRETE AVEC LES CTRL-C >-<");
                continue;
            } catch (Exception e) {
                resultHandler.handleResult(e);
                continue;
            }

            if (input == null) {
                break;
            }
            result = evaluate(input);
            if (result != NO_INPUT && !(result instanceof ExitRequest)) {
                resultHandler.handleResult(result);
            }
        }
    }
    @Override
    public Object evaluate(Input input) {
        if (noInput(input)) {
            return NO_INPUT;
        }

        if(ServControlApplication.getConsoleManager().isConsoleActive()){
            String raw = input.rawText();
            if(raw.toLowerCase().equals("exit") || raw.toLowerCase().equals("quit")){
                System.out.println();
                ServControlApplication.getConsoleManager().disableConsole();
            }else{
                try {
                    ServControlApplication.getConsoleManager().getActiveServer().writeInConsole(raw);
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.ERROR.log("Impossible de passer la commande au serveur !");
                }
            }
            return null;
        }

        String line = String.join(" ", input.words()).trim();
        String command = findLongestCommand(line);

        List<String> words = input.words();
        if (command != null) {
            MethodTarget methodTarget = methodTargets.get(command);
            Availability availability = methodTarget.getAvailability();
            if (availability.isAvailable()) {
                List<String> wordsForArgs = wordsForArguments(command, words);
                Method method = methodTarget.getMethod();

                Thread commandThread = Thread.currentThread();
                Object sh = Signals.register("INT", () -> Log.INFO.log("JE TE HAIS AVEC TES CTRL+C"));
                try {
                    Object[] args = resolveArgs(method, wordsForArgs);
                    validateArgs(args, methodTarget);
                    Object ret = ReflectionUtils.invokeMethod(method, methodTarget.getBean(), args);
                    System.out.println();
                    return ret;
                }
                catch (Exception e) {
                    return e;
                }finally {
                    Signals.unregister("INT", sh);
                }
            }
            else {
                return new CommandNotCurrentlyAvailable(command, availability);
            }
        }
        else {
            return new MNCommandNotFound(words);
        }
    }

    /**
     * Gather completion proposals given some (incomplete) input the user has already typed
     * in. When and how this method is invoked is implementation specific and decided by the
     * actual user interface.
     */
    public List<CompletionProposal> complete(CompletionContext context) {

        String prefix = context.upToCursor();

        List<CompletionProposal> candidates = new ArrayList<>(commandsStartingWith(prefix));

        String best = findLongestCommand(prefix);
        if (best != null) {
            CompletionContext argsContext = context.drop(best.split(" ").length);
            // Try to complete arguments
            MethodTarget methodTarget = methodTargets.get(best);
            Method method = methodTarget.getMethod();

            List<MethodParameter> parameters = Utils.createMethodParameters(method).collect(Collectors.toList());
            for (ParameterResolver resolver : parameterResolvers) {
                for (int index = 0; index < parameters.size(); index++) {
                    MethodParameter parameter = parameters.get(index);
                    if (resolver.supports(parameter)) {
                        candidates.addAll(resolver.complete(parameter, argsContext));
                    }
                }
            }
        }
        return candidates;
    }

    private List<CompletionProposal> commandsStartingWith(String prefix) {
        // Workaround for https://github.com/spring-projects/spring-shell/issues/150
        // (sadly, this ties this class to JLine somehow)
        int lastWordStart = prefix.lastIndexOf(' ') + 1;
        return methodTargets.entrySet().stream()
            .filter(e -> e.getKey().startsWith(prefix))
            .map(e -> toCommandProposal(e.getKey().substring(lastWordStart), e.getValue()))
            .collect(Collectors.toList());
    }

    private CompletionProposal toCommandProposal(String command, MethodTarget methodTarget) {
        return new CompletionProposal(command)
            .dontQuote(true)
            .category("Commandes disponibles")
            .description(methodTarget.getHelp());
    }

    private boolean noInput(Input input) {
        return input.words().isEmpty()
            || (input.words().size() == 1 && input.words().get(0).trim().isEmpty())
            || (input.words().iterator().next().matches("\\s*//.*"));
    }

    private void validateParameterResolvers(MethodTarget methodTarget) {
        Utils.createMethodParameters(methodTarget.getMethod())
            .forEach(parameter -> {
                parameterResolvers.stream()
                    .filter(resolver -> resolver.supports(parameter))
                    .findFirst()
                    .orElseThrow(() -> new ParameterResolverMissingException(parameter));
            });
    }

    private String findLongestCommand(String prefix) {
        String result = methodTargets.keySet().stream()
            .filter(command -> prefix.equals(command) || prefix.startsWith(command + " "))
            .reduce("", (c1, c2) -> c1.length() > c2.length() ? c1 : c2);
        return "".equals(result) ? null : result;
    }

    private List<String> wordsForArguments(String command, List<String> words) {
        int wordsUsedForCommandKey = command.split(" ").length;
        List<String> args = words.subList(wordsUsedForCommandKey, words.size());
        int last = args.size() - 1;
        if (last >= 0 && "".equals(args.get(last))) {
            args.remove(last);
        }
        return args;
    }

    private Object[] resolveArgs(Method method, List<String> wordsForArgs) {
        List<MethodParameter> parameters = Utils.createMethodParameters(method).collect(Collectors.toList());
        Object[] args = new Object[parameters.size()];
        Arrays.fill(args, UNRESOLVED);
        for (ParameterResolver resolver : parameterResolvers) {
            for (int argIndex = 0; argIndex < args.length; argIndex++) {
                MethodParameter parameter = parameters.get(argIndex);
                if (args[argIndex] == UNRESOLVED && resolver.supports(parameter)) {
                    args[argIndex] = resolver.resolve(parameter, wordsForArgs).resolvedValue();
                }
            }
        }
        return args;
    }

    private void validateArgs(Object[] args, MethodTarget methodTarget) {
        for (int i = 0; i < args.length; i++) {
            if (args[i] == UNRESOLVED) {
                MethodParameter methodParameter = Utils.createMethodParameter(methodTarget.getMethod(), i);
                throw new IllegalStateException("Could not resolve " + methodParameter);
            }
        }
        Set<ConstraintViolation<Object>> constraintViolations = validator.forExecutables().validateParameters(
            methodTarget.getBean(),
            methodTarget.getMethod(),
            args);
        if (constraintViolations.size() > 0) {
            throw new ParameterValidationException(constraintViolations, methodTarget);
        }
    }

    @Override
    @Autowired(required = false)
    public void setValidatorFactory(ValidatorFactory validatorFactory) {
        this.validator = validatorFactory.getValidator();
    }
}
