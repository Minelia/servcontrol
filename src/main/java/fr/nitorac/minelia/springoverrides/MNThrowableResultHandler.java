package fr.nitorac.minelia.springoverrides;

import org.jline.utils.AttributedString;
import org.jline.utils.AttributedStringBuilder;
import org.jline.utils.AttributedStyle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.shell.CommandRegistry;
import org.springframework.shell.jline.InteractiveShellApplicationRunner;
import org.springframework.shell.result.ThrowableResultHandler;
import org.springframework.util.StringUtils;

public class MNThrowableResultHandler extends ThrowableResultHandler {

    /**
     * The name of the command that may be used to print details about the last error.
     */
    public static final String DETAILS_COMMAND_NAME = "stacktrace";

    private Throwable lastError;

    @Autowired
    @Lazy
    private CommandRegistry commandRegistry;

    @Autowired @Lazy
    private InteractiveShellApplicationRunner interactiveRunner;

    @Override
    protected void doHandleResult(Throwable result) {
        lastError = result;
        String toPrint = StringUtils.hasLength(result.getMessage()) ? result.getMessage() : result.toString();
        terminal.writer().println(new AttributedString(toPrint,
            AttributedStyle.DEFAULT.foreground(AttributedStyle.RED)).toAnsi());
        if (interactiveRunner.isEnabled() && commandRegistry.listCommands().containsKey(DETAILS_COMMAND_NAME)) {
            terminal.writer().println(
                new AttributedStringBuilder()
                    .append("Les détails de l'erreur ne sont pas tous affichés. Vous pouvez utiliser la commande ", AttributedStyle.DEFAULT.foreground(AttributedStyle.RED))
                    .append(DETAILS_COMMAND_NAME, AttributedStyle.DEFAULT.foreground(AttributedStyle.RED).bold())
                    .append(" pour afficher l'erreur complète.", AttributedStyle.DEFAULT.foreground(AttributedStyle.RED))
                    .toAnsi()
            );
        }
        terminal.writer().flush();
        if (!interactiveRunner.isEnabled()) {
            if (result instanceof RuntimeException) {
                throw (RuntimeException) result;
            }
            else if (result instanceof Error) {
                throw (Error) result;
            }
            else {
                throw new RuntimeException((Throwable) result);
            }
        }
    }

    /**
     * Return the last error that was dealt with by this result handler.
     */
    public Throwable getLastError() {
        return lastError;
    }
}
