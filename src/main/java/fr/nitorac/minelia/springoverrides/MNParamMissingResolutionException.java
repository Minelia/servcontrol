package fr.nitorac.minelia.springoverrides;

import org.springframework.shell.ParameterDescription;
import org.springframework.shell.ParameterMissingResolutionException;
import org.springframework.shell.ParameterResolverMissingException;

public class MNParamMissingResolutionException extends ParameterMissingResolutionException {
    private final ParameterDescription parameterDescription;

    public MNParamMissingResolutionException(ParameterDescription parameterDescription) {
        super(parameterDescription);
        this.parameterDescription = parameterDescription;
    }

    public ParameterDescription getParameterDescription() {
        return parameterDescription;
    }

    @Override
    public String getMessage() {
        return String.format("Le paramètre '%s' doit être spécifié", parameterDescription);
    }
}
