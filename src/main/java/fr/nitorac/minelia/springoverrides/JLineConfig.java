package fr.nitorac.minelia.springoverrides;

import com.sun.scenario.effect.impl.prism.PrReflectionPeer;
import fr.nitorac.minelia.ServControlApplication;
import fr.nitorac.minelia.Utils;
import org.jline.reader.*;
import org.jline.reader.impl.history.DefaultHistory;
import org.jline.terminal.Attributes;
import org.jline.terminal.Terminal;
import org.jline.terminal.TerminalBuilder;
import org.jline.utils.AttributedString;
import org.jline.utils.AttributedStringBuilder;
import org.jline.utils.AttributedStyle;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.ansi.AnsiColor;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.Ordered;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.Environment;
import org.springframework.shell.*;
import org.springframework.shell.jline.*;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static org.springframework.shell.jline.InteractiveShellApplicationRunner.SPRING_SHELL_INTERACTIVE;
import static org.springframework.shell.jline.ScriptShellApplicationRunner.SPRING_SHELL_SCRIPT;

@AutoConfigureOrder(Ordered.HIGHEST_PRECEDENCE)
@Configuration
public class JLineConfig {

    @Autowired
    private PromptProvider promptProvider;

    @Autowired @Lazy
    private History history;

    @Autowired
    private Shell shell;

    @Bean(destroyMethod = "close")
    public Terminal terminal() {
        try {
            Terminal terminal = TerminalBuilder.builder()
                .name("Minelia SC")
                .build();
            return terminal;
        }
        catch (IOException e) {
            throw new BeanCreationException("Could not create Terminal: " + e.getMessage());
        }
    }

    @Bean
    @ConditionalOnProperty(prefix = SPRING_SHELL_INTERACTIVE, value = InteractiveShellApplicationRunner.ENABLED, havingValue = "true", matchIfMissing = true)
    public ApplicationRunner interactiveApplicationRunner(Parser parser, Environment environment) {
        return new InteractiveShellApplicationRunner(lineReader(), promptProvider, parser, shell, environment);
    }

    @Bean
    @ConditionalOnProperty(prefix = SPRING_SHELL_SCRIPT, value = ScriptShellApplicationRunner.ENABLED, havingValue = "true", matchIfMissing = true)
    public ApplicationRunner scriptApplicationRunner(Parser parser, ConfigurableEnvironment environment) {
        return new ScriptShellApplicationRunner(parser, shell, environment);
    }


    @Bean
    public PromptProvider promptProvider() {
        return Utils::getPrompt;
    }

    /**
     * Installs a default JLine history, and triggers saving to file on context shutdown. Filename is based on
     * {@literal spring.application.name}.
     *
     * @author Eric Bottard
     */
    @Configuration
    public static class HistoryConfiguration {

        @Autowired @Lazy
        private History history;

        @Bean
        public History history(LineReader lineReader, @Value("TEST.log") String historyPath) {
            lineReader.setVariable(LineReader.HISTORY_FILE, Paths.get(historyPath));
            return new HistoryNoSave(lineReader);
        }

        @EventListener
        public void onContextClosedEvent(ContextClosedEvent event) throws IOException {
            history.save();
        }
    }

    static class HistoryNoSave extends DefaultHistory{

    public HistoryNoSave(LineReader reader) {
        this.attach(reader);
    }

        @Override
        public void save(){
            // EMPTY x)
        }
    }

    @Bean
    public JLineShellAutoConfiguration.CompleterAdapter completer() {
        return new JLineShellAutoConfiguration.CompleterAdapter();
    }

    /*
     * Using setter injection to work around a circular dependency.
     */
    @PostConstruct
    public void lateInit() {
        completer().setShell(shell);
    }

    @Bean
    public Parser parser() {
        ExtendedDefaultParser parser = new ExtendedDefaultParser();
        parser.setEofOnUnclosedQuote(true);
        parser.setEofOnEscapedNewLine(true);
        return parser;
    }

    @Bean
    public LineReader lineReader() {
        LineReaderBuilder lineReaderBuilder = LineReaderBuilder.builder()
            .terminal(terminal())
            .appName("Minelia ServControl")
            .completer(completer())
            .history(history)
            .highlighter((reader, buffer) -> {
                if(ServControlApplication.getConsoleManager().isConsoleActive()){
                    return new AttributedString(buffer);
                }
                int l = 0;
                String best = null;
                for (String command : shell.listCommands().keySet()) {
                    if (buffer.startsWith(command) && command.length() > l) {
                        l = command.length();
                        best = command;
                    }
                }
                if (best != null) {
                    return new AttributedStringBuilder(buffer.length()).append(best, AttributedStyle.BOLD).append(buffer.substring(l)).toAttributedString();
                }
                else {
                    return new AttributedString(buffer, AttributedStyle.DEFAULT.foreground(AttributedStyle.BRIGHT | AttributedStyle.RED));
                }
            })
            .parser(parser());

        return lineReaderBuilder.build();
    }

    /**
     * Sanitize the buffer input given the customizations applied to the JLine parser (<em>e.g.</em> support for
     * line continuations, <em>etc.</em>)
     */
    static List<String> sanitizeInput(List<String> words) {
        words = words.stream()
            .map(s -> s.replaceAll("^\\n+|\\n+$", "")) // CR at beginning/end of line introduced by backslash continuation
            .map(s -> s.replaceAll("\\n+", " ")) // CR in middle of word introduced by return inside a quoted string
            .collect(Collectors.toList());

        return words;
    }

    /**
     * A bridge between JLine's {@link Completer} contract and our own.
     * @author Eric Bottard
     */
    public static class CompleterAdapter implements Completer {

        private Shell shell;

        @Override
        public void complete(LineReader reader, ParsedLine line, List<Candidate> candidates) {
            CompletingParsedLine cpl = (line instanceof CompletingParsedLine) ? ((CompletingParsedLine) line) : t -> t;

            CompletionContext context = new CompletionContext(sanitizeInput(line.words()), line.wordIndex(), line.wordCursor());

            List<CompletionProposal> proposals = shell.complete(context);
            proposals.stream()
                .map(p -> new Candidate(
                    p.dontQuote() ? p.value() : cpl.emit(p.value()).toString(),
                    p.displayText(),
                    p.category(),
                    p.description(),
                    null,
                    null,
                    true)
                )
                .forEach(candidates::add);
        }

        public void setShell(Shell shell) {
            this.shell = shell;
        }
    }

    class ExtendedDefaultParser implements Parser {

        private char[] quoteChars = { '\'', '"' };

        private char[] escapeChars = { '\\' };

        private boolean eofOnUnclosedQuote;

        private boolean eofOnEscapedNewLine;

        public void setQuoteChars(final char[] chars) {
            this.quoteChars = chars;
        }

        public char[] getQuoteChars() {
            return this.quoteChars;
        }

        public void setEscapeChars(final char[] chars) {
            this.escapeChars = chars;
        }

        public char[] getEscapeChars() {
            return this.escapeChars;
        }

        public void setEofOnUnclosedQuote(boolean eofOnUnclosedQuote) {
            this.eofOnUnclosedQuote = eofOnUnclosedQuote;
        }

        public boolean isEofOnUnclosedQuote() {
            return eofOnUnclosedQuote;
        }

        public void setEofOnEscapedNewLine(boolean eofOnEscapedNewLine) {
            this.eofOnEscapedNewLine = eofOnEscapedNewLine;
        }

        public boolean isEofOnEscapedNewLine() {
            return eofOnEscapedNewLine;
        }

        public ParsedLine parse(final String line, final int cursor, ParseContext context) {
            List<String> words = new LinkedList<>();
            StringBuilder current = new StringBuilder();
            int wordCursor = -1;
            int wordIndex = -1;
            int quoteStart = -1;

            for (int i = 0; (line != null) && (i < line.length()); i++) {
                // once we reach the cursor, set the
                // position of the selected index
                if (i == cursor) {
                    wordIndex = words.size();
                    // the position in the current argument is just the
                    // length of the current argument
                    wordCursor = current.length();
                }

                if (quoteStart < 0 && isQuoteChar(line, i)) {
                    // Start a quote block
                    quoteStart = i;
                }
                else if (quoteStart >= 0) {
                    // In a quote block
                    if (line.charAt(quoteStart) == line.charAt(i) && !isEscaped(line, i)) {
                        // End the block; arg could be empty, but that's fine
                        words.add(current.toString());
                        current.setLength(0);
                        quoteStart = -1;
                    }
                    else if (!isEscapeChar(line, i)) {
                        // Take the next character
                        current.append(line.charAt(i));
                    }
                }
                else {
                    // Not in a quote block
                    if (isDelimiter(line, i)) {
                        if (current.length() > 0) {
                            words.add(current.toString());
                            current.setLength(0); // reset the arg
                        }
                    }
                    else if (!isEscapeChar(line, i)) {
                        current.append(line.charAt(i));
                    }
                }
            }

            if (current.length() > 0 || cursor == line.length()) {
                words.add(current.toString());
            }

            if (cursor == line.length()) {
                wordIndex = words.size() - 1;
                wordCursor = words.get(words.size() - 1).length();
            }

            if (eofOnEscapedNewLine && isEscapeChar(line, line.length() - 1)) {
                throw new EOFError(-1, -1, "Escaped new line", "newline");
            }
            if (eofOnUnclosedQuote && quoteStart >= 0 && context != ParseContext.COMPLETE) {
                throw new EOFError(-1, -1, "Missing closing quote", line.charAt(quoteStart) == '\'' ? "quote" : "dquote");
            }

            String openingQuote = quoteStart >= 0 ? line.substring(quoteStart, quoteStart + 1) : null;
            return new ExtendedDefaultParser.ExtendedArgumentList(line, words, wordIndex, wordCursor, cursor, openingQuote);
        }

        /**
         * Returns true if the specified character is a whitespace parameter. Check to ensure
         * that the character is not escaped by any of {@link #getQuoteChars}, and is not
         * escaped by ant of the {@link #getEscapeChars}, and returns true from
         * {@link #isDelimiterChar}.
         *
         * @param buffer The complete command buffer
         * @param pos The index of the character in the buffer
         * @return True if the character should be a delimiter
         */
        public boolean isDelimiter(final CharSequence buffer, final int pos) {
            return !isQuoted(buffer, pos) && !isEscaped(buffer, pos) && isDelimiterChar(buffer, pos);
        }

        public boolean isQuoted(final CharSequence buffer, final int pos) {
            return false;
        }

        public boolean isQuoteChar(final CharSequence buffer, final int pos) {
            if (pos < 0) {
                return false;
            }

            for (int i = 0; (quoteChars != null) && (i < quoteChars.length); i++) {
                if (buffer.charAt(pos) == quoteChars[i]) {
                    return !isEscaped(buffer, pos);
                }
            }

            return false;
        }

        /**
         * Check if this character is a valid escape char (i.e. one that has not been escaped)
         */
        public boolean isEscapeChar(final CharSequence buffer, final int pos) {
            if (pos < 0) {
                return false;
            }

            for (int i = 0; (escapeChars != null) && (i < escapeChars.length); i++) {
                if (buffer.charAt(pos) == escapeChars[i]) {
                    return !isEscaped(buffer, pos); // escape escape
                }
            }

            return false;
        }

        /**
         * Check if a character is escaped (i.e. if the previous character is an escape)
         *
         * @param buffer the buffer to check in
         * @param pos the position of the character to check
         * @return true if the character at the specified position in the given buffer is an
         * escape character and the character immediately preceding it is not an escape
         * character.
         */
        public boolean isEscaped(final CharSequence buffer, final int pos) {
            if (pos <= 0) {
                return false;
            }

            return isEscapeChar(buffer, pos - 1);
        }

        /**
         * Returns true if the character at the specified position if a delimiter. This method
         * will only be called if the character is not enclosed in any of the
         * {@link #getQuoteChars}, and is not escaped by ant of the {@link #getEscapeChars}.
         * To perform escaping manually, override {@link #isDelimiter} instead.
         */
        public boolean isDelimiterChar(CharSequence buffer, int pos) {
            return Character.isWhitespace(buffer.charAt(pos));
        }

        /**
         * The result of a delimited buffer.
         *
         * @author <a href="mailto:mwp1@cornell.edu">Marc Prud'hommeaux</a>
         */
        public class ExtendedArgumentList implements ParsedLine, CompletingParsedLine {
            private final String line;

            private final List<String> words;

            private final int wordIndex;

            private final int wordCursor;

            private final int cursor;

            private final String openingQuote;

            public ExtendedArgumentList(final String line, final List<String> words, final int wordIndex,
                                        final int wordCursor, final int cursor, final String openingQuote) {
                this.line = line;
                this.words = Collections.unmodifiableList(Objects.requireNonNull(words));
                this.wordIndex = wordIndex;
                this.wordCursor = wordCursor;
                this.cursor = cursor;
                this.openingQuote = openingQuote;
            }

            public int wordIndex() {
                return this.wordIndex;
            }

            public String word() {
                // TODO: word() should always be contained in words()
                if ((wordIndex < 0) || (wordIndex >= words.size())) {
                    return "";
                }
                return words.get(wordIndex);
            }

            public int wordCursor() {
                return this.wordCursor;
            }

            public List<String> words() {
                return this.words;
            }

            public int cursor() {
                return this.cursor;
            }

            public String line() {
                return line;
            }

            @Override
            public CharSequence emit(CharSequence candidate) {
                StringBuilder sb = new StringBuilder(candidate);
                Predicate<Integer> needToBeEscaped;
                // Completion is protected by an opening quote:
                // Delimiters (spaces) don't need to be escaped, nor do other quotes, but everything else does.
                // Also, close the quote at the end
                if (openingQuote != null) {
                    needToBeEscaped = i -> isRawEscapeChar(sb.charAt(i)) || String.valueOf(sb.charAt(i)).equals(openingQuote);
                } // No quote protection, need to escape everything: delimiter chars (spaces), quote chars
                // and escapes themselves
                else {
                    needToBeEscaped = i -> isDelimiterChar(sb, i) || isRawEscapeChar(sb.charAt(i)) || isRawQuoteChar(sb.charAt(i));
                }
                for (int i = 0; i < sb.length(); i++) {
                    if (needToBeEscaped.test(i)) {
                        sb.insert(i++, escapeChars[0]);
                    }
                }
                if (openingQuote != null) {
                    sb.append(openingQuote);
                }
                return sb;
            }
        }

        private boolean isRawEscapeChar(char key) {
            for (char e : escapeChars) {
                if (e == key) {
                    return true;
                }
            }
            return false;
        }

        private boolean isRawQuoteChar(char key) {
            for (char e : quoteChars) {
                if (e == key) {
                    return true;
                }
            }
            return false;
        }

    }
}
