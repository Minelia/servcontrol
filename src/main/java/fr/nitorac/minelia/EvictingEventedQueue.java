package fr.nitorac.minelia;


import com.google.common.collect.EvictingQueue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class EvictingEventedQueue {

    private EvictingQueue<String> lastLines;
    private Map<String, OnLineAdded> callbacks;

    public EvictingEventedQueue() {
        lastLines = EvictingQueue.create(250);
        callbacks = new HashMap<>();
    }

    public void addListener(String key, OnLineAdded callback){
        callbacks.put(key, callback);
    }

    public boolean removeListener(String key){
        return callbacks.remove(key) != null;
    }

    public void removeAllListeners(){
        callbacks.clear();
    }

    public void push(String line){
        lastLines.add(line);
        callbacks.values().forEach(callback -> callback.lineAdded(line));
    }

    public void clear(){
        lastLines.clear();
    }

    public ArrayList<String> getAllHistory(){
        return new ArrayList<>(lastLines);
    }

    public interface OnLineAdded{
        void lineAdded(String line);
    }
}
