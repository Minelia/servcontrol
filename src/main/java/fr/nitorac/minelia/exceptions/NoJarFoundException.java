package fr.nitorac.minelia.exceptions;

import java.io.File;

public class NoJarFoundException extends Exception {

    private File baseDir;

    public NoJarFoundException(File baseDir){
        this.baseDir = baseDir;
    }

    @Override
    public String getMessage(){
        return "Aucun jar ne se trouve dans le dossier : " + baseDir.getAbsolutePath();
    }
}
