package fr.nitorac.minelia;

import org.springframework.boot.ansi.AnsiColor;

import java.util.Arrays;

/**
 * Created by Nitorac on 17/07/2017.
 */
public enum Log {
    DEBUG("[DEBUG] : ", AnsiColor.CYAN),
    INFO("[INFO] : ", AnsiColor.GREEN),
    WARN("[WARN] : ", AnsiColor.YELLOW),
    ERROR("[ERROR] : ", AnsiColor.RED);

    String prefix;
    AnsiColor color;

    Log(String prefix, AnsiColor color){
        this.prefix = prefix;
        this.color = color;
    }

    public void logPrompt(String line){
        Utils.log(getLog(line));
    }

    public String getLog(String line){
        String date = "[" + Utils.getNowFormatted("HH:mm:ss") + "] ";
        return Utils.ansi(AnsiColor.DEFAULT) + date + Utils.ansi(color) + prefix + line + Utils.ansi(AnsiColor.DEFAULT);
    }

    public void log(String line){
        System.out.println(getLog(line));
    }

    public void log(String... lines){
        for(String str : lines){
            log(str);
        }
    }

    public void newLine(int nbOfNewLines){
        for(int i = 0; i < nbOfNewLines; i++){
            log("");
        }
    }

    public void newLine(){
        newLine(1);
    }
}
