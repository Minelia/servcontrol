package fr.nitorac.minelia;


import fr.nitorac.minelia.exceptions.NoJarFoundException;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Nitorac on 17/07/2017.
 */
public class Server {

    public static final String BIND_LISTENER_KEY = "BindScreen";
    public static final String QUIT_CONSOLE_ON_SERVER_EXIT = "QuiConsoleOnServExit";

    private File baseDir;
    private File jar;
    private Process process;
    private OutputMonitor outputMonitor;
    private AliveMonitor aliveMonitor;
    private EvictingEventedQueue log;
    private int xmxM;
    private int xmsM;

    public Server(File baseDir) throws NoJarFoundException {
        this.baseDir = baseDir;
        File jar;
        if((jar = Utils.getFirstJarInDir(baseDir)) == null){
            throw new NoJarFoundException(baseDir);
        }
        this.jar = jar;
    }

    public Server(String baseDir) throws NoJarFoundException {
        this(new File(baseDir));
    }

    public boolean isAlive(){
        return (process != null) && process.isAlive();
    }

    public void startServer() throws IOException{
        startServer(2048, 2048);
    }

    public void startServer(int xmxM, int xmsM) throws IOException{
        if(isAlive()) return;

        ProcessBuilder pb = constructProcess(xmxM, xmsM);
        process = pb.start();
        startOutputMonitor();

        aliveMonitor = new AliveMonitor();
        aliveMonitor.addCallback(QUIT_CONSOLE_ON_SERVER_EXIT, () -> {
            String key = Utils.getStringFromServ(Server.this);
            ServControlApplication.getConsoleManager().disableConsole(key);
            Log.INFO.logPrompt("Le server " + key + " s'est éteint !");
        });
        aliveMonitor.start();
    }

    public ProcessBuilder constructProcess(int xmxM, int xmsX) throws IOException {
        ProcessBuilder pb = new ProcessBuilder("java", "-jar", "-Djline.terminal=jline.UnsupportedTerminal", "-Xmx" + xmxM + "M", "-Xms" + xmsX + "M", jar.getName());
        File log = new File(baseDir + File.separator + baseDir.getName() + ".log");
        if(!log.exists()){
            log.createNewFile();
        }
        pb.directory(baseDir);
        pb.redirectErrorStream(true);
        //Log.DEBUG.log("Directory: " + pb.directory().getAbsolutePath());
        return pb;
    }

    public void stopServer() throws IOException {
        writeInConsole("stop");
        writeInConsole("end");
        writeInConsole("stop");
        writeInConsole("end");
    }

    public void writeInConsole(String command) throws IOException{
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(process.getOutputStream(), "Cp1252"));
        bw.write(command);
        bw.newLine();
        bw.flush();
    }

    public AliveMonitor getAliveMonitor(){
        return aliveMonitor;
    }

    public void startOutputMonitor(){
        outputMonitor = new OutputMonitor();
        outputMonitor.start();
    }

    public void bindOutputToScreen(boolean printHistory){
        if(printHistory) log.getAllHistory().forEach(System.out::println);
        log.addListener(BIND_LISTENER_KEY, line -> {
            Utils.log(line);
        });
    }

    public void unbindOutputToScreen(){
        log.removeListener(BIND_LISTENER_KEY);
    }

    public void stopOutputMonitor(){
        if(outputMonitor.isAlive()){
            outputMonitor.interrupt();
        }
    }

    public void clearOutputMonitor(){
        outputMonitor.clear();
    }

    public class AliveMonitor extends Thread{

        private Map<String, Runnable> callbacks;

        public void addCallback(String key, Runnable callback){
            callbacks.put(key, callback);
        }

        public void removeCallback(String key){
            callbacks.remove(key);
        }

        public void removeAll(){
            callbacks.clear();
        }

        public AliveMonitor(){
            this.callbacks = new HashMap<>();
        }

        @Override
        public void run(){
            try {
                process.waitFor();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            callbacks.values().forEach(Runnable::run);
            clearOutputMonitor();
        }
    }

    public class OutputMonitor extends Thread{

        private boolean isInterrupted = false;

        public boolean isDead(){
            return isInterrupted;
        }

        @Override
        public void interrupt(){
            isInterrupted = true;
        }

        public void clear(){
            if(log != null){
                log.clear();
            }
        }

        @Override
        public void run(){
            if(log == null){
                log = new EvictingEventedQueue();
            }

            InputStream out = process.getInputStream();

            byte[] buffer = new byte[4096];
            while (!isInterrupted) {
                try{
                    int no = out.available();
                    if (no > 0) {
                        int n = out.read(buffer, 0, Math.min(no, buffer.length));
                        String line = (new String(buffer, 0, n)).trim();
                        if(!line.isEmpty() && !line.startsWith(">")){
                            if(line.endsWith("\n>")){
                                log.push(line.substring(0, line.length()-2));
                            }else{
                                log.push(line);
                            }
                        }
                    }
                }catch (IOException  e){
                    Log.WARN.log("Le lecteur de log du serveur " + Utils.getStringFromServ(Server.this) + " a loupé une ligne");
                    e.printStackTrace();
                }

                try {
                    Thread.sleep(100);
                }
                catch (InterruptedException ignored) {

                }
            }
        }
    }
}
