package fr.nitorac.minelia;

/**
 * Created by Nitorac on 17/07/2017.
 */
public class Csts {
    public static final String REDIS_MAIN_CHANNEL = "Main";

    public static final String REDIS_TYPE_JSONKEY = "type";
    public static final String REDIS_MSG_JSONKEY = "msg";
    public static final String REDIS_TITLE_JSONKEY = "title";

    /********CONFIG*******/
    public static final String GENERAL_CONF = "general.yml";
}
