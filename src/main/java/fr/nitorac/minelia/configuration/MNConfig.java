package fr.nitorac.minelia.configuration;

import fr.nitorac.minelia.Log;
import fr.nitorac.minelia.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nitorac on 17/07/2017.
 */
public class MNConfig {
    private File location;
    private boolean loaded = false;
    private YamlConfiguration config;

    public MNConfig(File loc) {
        this.location = loc;
        this.config = new YamlConfiguration();
        load();
    }

    public MNConfig(String path) {
        this.location = new File(path);
        this.config = new YamlConfiguration();
        load();
    }

    /************************
     *          Sauvegarde la configuration dans le fichier
     */
    public void save(){
        try{
            config.save(location);
        }catch (Exception e){
            Log.ERROR.log("Impossible d'écrire la config " + location.getName() + " (" + e.getMessage() + ")");
        }
    }

    public void flush(){
        config.getKeys(false).forEach(key -> config.set(key, null));
    }

    /********************************
     *          Charge le fichier de config
     *          /!\ Le nom du fichier de sortie doit être le même que celui situé à la racine (voir rank.yml pour exemple)
     */
    public MNConfig load(){
        if (location.exists()) {
            try {
                config.load(location);
                Log.INFO.log(location.getName() + " charge ! {" + location.getCanonicalPath() + "}");
            } catch (Exception e) {
                Log.ERROR.log("Configuration : " + e.getLocalizedMessage());
            }
            loaded = true;
        } else {
            try {
                location.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return this;
    }

    public String getName(){
        return location.getName();
    }

    public YamlConfiguration getYAMLConfig(){
        return config;
    }

    public File getFile(){
        return location;
    }

    public ArrayList<String> getSection(String section, boolean getKeys) {
        return new ArrayList<>(config.getConfigurationSection(section).getKeys(getKeys));
    }

    public void setFirstTimeAndSave(String path, Object defaut){
        if(!config.contains(path)){
            config.set(path, defaut);
            save();
        }
    }

    //    /!\ QUELQUES OVERRIDE CI DESSOUS SONT DIRECTEMENT DÉFINIS DANS LA CLASSE YAMLCONFIGURATION DE BUKKIT /!\
    public String getString(String path, String defaut) {
        setFirstTimeAndSave(path, defaut);
        return config.getString(path);
    }

    public List<String> getStringList(String path, List<String> defaut) {
        setFirstTimeAndSave(path, defaut);
        return config.getStringList(path);
    }

    public Integer getInt(String path, Integer defaut) {
        setFirstTimeAndSave(path, defaut);
        return config.getInt(path, defaut);
    }

    public List<Integer> getIntList(String path, List<Integer> defaut) {
        setFirstTimeAndSave(path, defaut);
        return config.getIntegerList(path);
    }

    public Boolean getBoolean(String path, Boolean defaut) {
        setFirstTimeAndSave(path, defaut);
        return config.getBoolean(path, defaut);
    }

    public List<Boolean> getBooleanList(String path, List<Boolean> defaut) {
        setFirstTimeAndSave(path, defaut);
        return config.getBooleanList(path);
    }

    public Long getLong(String path, Long defaut) {
        setFirstTimeAndSave(path, defaut);
        return config.getLong(path, defaut);
    }

    public List<Long> getLongList(String path, List<Long> defaut) {
        setFirstTimeAndSave(path, defaut);
        return config.getLongList(path);
    }

    public Float getFloat(String path, Float defaut) {
        setFirstTimeAndSave(path, defaut);
        return (float)config.getDouble(path, (double)defaut);
    }

    public List<Float> getFloatList(String path, List<Float> defaut) {
        setFirstTimeAndSave(path, defaut);
        return config.getFloatList(path);
    }

    public Short getShort(String path, Short defaut) {
        setFirstTimeAndSave(path, defaut);
        return (Short)config.get(path, defaut);
    }

    public List<Short> getShortList(String path, List<Short> defaut) {
        setFirstTimeAndSave(path, defaut);
        return config.getShortList(path);
    }

    public Double getDouble(String path, Double defaut) {
        setFirstTimeAndSave(path, defaut);
        return config.getDouble(path, defaut);
    }

    public List<Double> getDoubleList(String path, List<Double> defaut) {
        setFirstTimeAndSave(path, defaut);
        return config.getDoubleList(path);
    }

    public Object getObject(String path, Object defaut) {
        setFirstTimeAndSave(path, defaut);
        return config.get(path);
    }

    public List<?> getObjectList(String path, List<?> defaut) {
        setFirstTimeAndSave(path, defaut);
        return config.getList(path, defaut);
    }

    public void setObject(String path, Object value) {
        config.set(path, value);
    }

    public void reload() {
        try {
            config.load(location);
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }

    public boolean isLoaded(){
        return loaded;
    }
}
