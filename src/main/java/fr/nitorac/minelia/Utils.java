package fr.nitorac.minelia;

import org.jline.utils.AttributedString;
import org.springframework.boot.ansi.AnsiColor;
import org.springframework.util.StringUtils;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.Set;

/**
 * Created by Nitorac on 17/07/2017.
 */
public class Utils {

    public static File currentDir = new File(".");

    public Utils() throws Exception{
        pluginLoc = new File(new File(".").getCanonicalPath() + File.separator + "plugins");
        serverLoc = pluginLoc.getParentFile();
        rootLoc = serverLoc.getParentFile();
    }

    //  Dossier du serveur actuel ( /MineliaServer/build ou /MineliaServer/detente ou /MineliaServer/dev )
    public File serverLoc;

    // Dossier plugins du serveur actuel ( /MineliaServer/build/plugins ou /MineliaServer/detente/plugins ou /MineliaServer/dev/plugins )
    public File pluginLoc;

    // Dossier de l'ensemble des serveurs ( /MineliaServer )
    public File rootLoc;

    public static File getFirstJarInDir(File dir) {
        File[] jars;
        return ((jars = dir.listFiles((dir1, name) -> name.toLowerCase().endsWith(".jar"))) != null && jars.length > 0) ? jars[0] : null;
    }

    public static int getPosFromItemMax(Set<String> str){
        return str.stream().mapToInt(String::length).max().orElse(-1);
    }

    public static String getStringFromServ(Server s){
        return ServControlApplication.servers.entrySet().stream().filter(entry -> entry.getValue().equals(s)).map(Map.Entry::getKey).findFirst().get();
    }

    public static String ansi(AnsiColor c){
        return "\u001B[" + c.toString() + "m";
    }

    public static String getNowFormatted(String pattern){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(pattern);
        LocalDateTime now = LocalDateTime.now();
        return dtf.format(now);
    }

    public static void log(String line){
        for(int i = 0; i < getPrompt().toString().length(); i++){
            System.out.print("\b");
        }
        System.out.println(line);
        System.out.print(getPrompt().toAnsi());
    }

    public static AttributedString getPrompt(){
        return AttributedString.fromAnsi("\033[33m[" +
            ((ServControlApplication.getConsoleManager().isConsoleActive()) ? "\033[32m" + StringUtils.capitalize(ServControlApplication.getConsoleManager().getActiveServerName()) : "\033[36mServControl") +
            "\033[33m]\033[0m>");
    }

    public static boolean hasValIn2Decomp(int baseNum, int toFind) {
        for (int i = 0; i < 31; i++) {
            int puissance = 1 << i;
            if (((baseNum >>> i) & 1) != 0 && puissance == toFind)
                return true;
            if (puissance > baseNum)
                return false;
        }
        return false;
    }
}

